import re
import pyperclip
import webbrowser

# 打開指定的網站
webbrowser.open("https://eip.systex.com/UOF/")

# 等待你在該網站複製資料後再執行分析處理
input("請在網站上複製資料後按 Enter 鍵繼續...")
# 從剪貼簿讀取資料
data = pyperclip.paste()

# 定義日期和時間的正則表達式
pattern = r"(\d{2}/\d{2}) \((.)\)(?: 補)?(?: (\d{2}:\d{2}) ~ (\d{2}:\d{2}))?"
work_pattern = r"工作地點 : .*"

# 分析資料並生成表格
attendance = []
for line in data.strip().split("\n"):
    line = line.replace("補", "")  # 如果有補字，先去除
    match = re.match(pattern, line.strip())
    if match:
        groups = match.groups()
        print("Groups:", groups)  # 添加調試語句，檢查匹配的分組
        if len(groups) == 4:
            date, day, start_time, end_time = groups
            day = day.strip()
            start_time = start_time if start_time else ""
            end_time = end_time if end_time else ""
            time_info = ["8", "0", "0", "0"] if start_time else ["", "", "", ""]
            attendance.append([int(date.split('/')[1]), day, start_time, end_time] + time_info)
        else:
            print("Error: Unexpected number of groups:", groups)
    elif re.match(work_pattern, line.strip()):  # Handle lines containing '工作地點'
        continue
    else:  # Handle lines containing '補'
        parts = re.findall(r"[\w']+", line)  # 使用正則表達式以處理不同格式的補充行
        print("Parts:", parts)  # 添加調試語句，檢查補充信息的分割結果
        if len(parts) == 5:
            _, date, day, start_time, end_time = parts
            time_info = ["8", "0", "0", "0"] if start_time else ["", "", "", ""]
            attendance.append([int(date.split('/')[1]), day.strip(), start_time, end_time] + time_info)
        else:
            print("Error: Unexpected number of parts:", parts)

# 將表格按照日期排序
attendance.sort(key=lambda x: x[0])

# 將表格內容拼接成字串，便於複製
table_str = "\n".join("\t".join(map(str, entry)) for entry in attendance)

# 輸出表格內容並複製到剪貼簿
pyperclip.copy(table_str)
print("以下內容已複製到剪貼簿：")
print(table_str)
