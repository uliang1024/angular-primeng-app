import { Component, HostListener } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ThemeService } from '../theme.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent {
  items: MenuItem[] | undefined;
  isHeaderActive: boolean = false;
  isHeaderSticky: boolean = false;

  value1: string = 'lara-light-blue';
  value2: string = 'lara-dark-blue';
  currentValue: string = this.value1;


  @HostListener('window:scroll', [])
  onWindowScroll() {
    this.isHeaderSticky = (window.scrollY > 0);
  }

  toggleHeaderClass() {
    this.isHeaderActive = !this.isHeaderActive;
  }

  constructor(private themeService: ThemeService) { }

  changeTheme() {
    this.currentValue = (this.currentValue === this.value1) ? this.value2 : this.value1;
    this.themeService.switchTheme(this.currentValue);
  }
}
