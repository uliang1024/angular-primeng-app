import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { FormsModule } from '@angular/forms';
import { StartComponent } from './start/start.component';
import { ConfigComponent } from './config/config.component';
import { MenubarModule } from 'primeng/menubar';
import { MainModule } from './main/main.module';
import { CoreModule } from './core/core.module';
import { SidebarService } from './core/components/sidebar/sidebar.service';

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    ConfigComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MainModule,
    BrowserAnimationsModule,
    HttpClientModule,
    InputTextModule,
    CheckboxModule,
    ButtonModule,
    RadioButtonModule,
    InputTextareaModule,
    DropdownModule,
    FormsModule,
    MenubarModule,
  ],
  providers: [SidebarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
