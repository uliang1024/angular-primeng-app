import { Component, HostListener, OnInit } from '@angular/core';
import { SidebarService } from '../sidebar/sidebar.service';

interface City {
  name: string;
  code: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  cities: City[] | undefined;
  selectedCity: City | undefined;

  sidebarVisible2: boolean = false;
  
  isHeaderSticky: boolean = false;

  constructor(private sidebarService: SidebarService) {}

  ngOnInit() {
    this.cities = [
      { name: 'New York', code: 'NY' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' }
    ];
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    this.isHeaderSticky = (window.scrollY > 0);
  }

  toggleSidebar() {
    this.sidebarService.toggleSidebar();
  }

}
